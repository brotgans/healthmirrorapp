//
//  RatingLong.swift
//  ControllVideos
//
//  Created by Ferdinand Sorg on 22.01.20.
//  Copyright © 2020 Ferdinand Sorg. All rights reserved.
//

import UIKit
import Foundation

class RatingLong: UIViewController, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var questionCollectionView: UICollectionView!
    @IBOutlet weak var submitRatingLong: UIButton!

    
    var listOfQuestions = [
        "Constateer je ander gedrag bij je cliënt?",
        "Welk ziektebeeld heeft je cliënt?",
        "Zijn er dingen die je opvielen?",
        "Heb je nog opmerkingen?"
    ]
    

    var listOfDescriptions = [
        "Bij meerdere antwoorden graag scheiden met een komma. \n Suggesties bij onbegrepen gedrag: angst, agitatie, apathie, depressie, claimend gedrag, negativisme, hallucinaties, decorumverlies, etc. \n Suggesties bij positief gedrag: lachen, doorgaan met handeling, afmaken van de handeling, enthousiast zijn, vriendelijk zijn, etc.",
        "Bij meerdere antwoorden graag scheiden met een komma",
        "Bij meerdere antwoorden graag scheiden met een komma",
        "Bij meerdere antwoorden graag scheiden met een komma",
        "Bij meerdere antwoorden graag scheiden met een komma"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.questionCollectionView.delegate = self
        self.questionCollectionView.dataSource = self
        self.questionCollectionView.allowsSelection = true
        
        self.questionCollectionView.register(UINib(nibName: "Questions", bundle: nil), forCellWithReuseIdentifier: "Questions")
        
        submitRatingLong.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)
    }
    
    @objc func buttonTapped(sender: UIButton) {
        
        questionCollectionView.reloadData()
        
        let finalRating = UserDefaults.standard.array(forKey: "QuestionRating") as? [String] ?? [String]()
        
        checkQuestionData()
        
        print("submitted with Rating: \(finalRating)")
        
//        self.dismiss(animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "startScreen") as UIViewController
        vc.modalTransitionStyle = .flipHorizontal
        
        present(vc, animated: true, completion: nil)
    }
    
}

extension RatingLong: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfQuestions.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "questionCell", for: indexPath) as! Questions

        // show question
        cell.questionLine.text = listOfQuestions[indexPath.row]
        
        cell.textView.text = listOfDescriptions[indexPath.row]
        cell.textView.textColor = UIColor.lightGray
        cell.textView.isEditable = false
        cell.textView.textAlignment = .center
        
        if(indexPath.row == 0) {
            cell.textView.sizeToFit()
        }

        cell.input.tag = indexPath.row
//        cell.LabelLeft.text = listOfLeft[indexPath.row]
//        cell.LabelRight.text = listOfRight[indexPath.row]

        // tag rating with ID
//        cell.questionRating.tag = indexPath.row
//        cell.inputView?.tag = indexPath.row
        

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("you selected \(indexPath.item)")
    }
    
}
