//
//  RatingShort.swift
//  ControllVideos
//
//  Created by Ferdinand Sorg on 22.01.20.
//  Copyright © 2020 Ferdinand Sorg. All rights reserved.
//

import UIKit
import Foundation

class RatingShort: UIViewController {
    

    @IBOutlet weak var nmgOne: UISegmentedControl!
    @IBOutlet weak var nmgTwo: UISegmentedControl!
    @IBOutlet weak var nmgThree: UISegmentedControl!
    
    @IBOutlet weak var submitRating: UIButton!
    
    @IBOutlet weak var extraQuestionLine: UILabel!
    @IBOutlet weak var extraButtonYes: UIButton!
    @IBOutlet weak var extraButtonNo: UIButton!
    
    @IBOutlet weak var extraQuestionLine2: UILabel!
    @IBOutlet weak var extraButtonYes2: UIButton!
    @IBOutlet weak var extraButtonNo2: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Reset float rating view's background color
//        self.nmgOne.backgroundColor = UIColor.clear
//        self.nmgOne.delegate = self
//        self.nmgOne.contentMode = UIView.ContentMode.scaleAspectFit
        //self.nmgOne.type = .wholeRatings
        
//        self.floatRatingViewTwo.backgroundColor = UIColor.clear
//        self.floatRatingViewTwo.delegate = self
//        self.floatRatingViewTwo.contentMode = UIView.ContentMode.scaleAspectFit
//        self.floatRatingViewTwo.type = .wholeRatings
//
//        self.floatRatingViewThree.backgroundColor = UIColor.clear
//        self.floatRatingViewThree.delegate = self
//        self.floatRatingViewThree.contentMode = UIView.ContentMode.scaleAspectFit
//        self.floatRatingViewThree.type = .wholeRatings
//
        submitRating.addTarget(self, action: #selector(self.submitButtonTapped), for: .touchUpInside)
        
        let id = UserDefaults.standard.integer(forKey: "videoID")
        let videoMeta = ViewController().getVideoMeta(id: id)
        
        var extraQuestion: String = ""
        var extraQuestion2: String = ""
        
        if videoMeta.count >= 4 {
            print("videoMeta has \(videoMeta.count)")
            extraQuestion = videoMeta[3] as! String
            extraQuestion2 = videoMeta[4] as! String
        }
        
        // safe Question in UserDefaults
        UserDefaults.standard.set(extraQuestion, forKey: "extraQuestionOne")
        UserDefaults.standard.set("", forKey: "extraAnswerOne")
        
        // safe Question 2 in UserDefaults
        UserDefaults.standard.set(extraQuestion2, forKey: "extraQuestionTwo")
        UserDefaults.standard.set("", forKey: "extraAnswerTwo")
        
        // first extra question
        if extraQuestion == "" {
            extraButtonYes.isHidden = true
            extraButtonNo.isHidden = true
            extraQuestionLine.isHidden = true
        } else {
            extraQuestionLine.text = extraQuestion
            extraButtonYes.layer.cornerRadius = 10.0
            extraButtonNo.layer.cornerRadius = 10.0
            extraButtonNo.backgroundImage(for: UIControl.State.selected)
            extraButtonYes.addTarget(self, action: #selector(self.extraButtonTapped), for: .touchUpInside)
            extraButtonNo.addTarget(self, action: #selector(self.extraButtonTapped), for: .touchUpInside)
        }
        
        // second extra question
        if extraQuestion2 == "" {
            extraButtonYes2.isHidden = true
            extraButtonNo2.isHidden = true
            extraQuestionLine2.isHidden = true
        } else {
            extraQuestionLine2.text = extraQuestion2
            extraButtonYes2.layer.cornerRadius = 10.0
            extraButtonNo2.layer.cornerRadius = 10.0
            extraButtonNo2.backgroundImage(for: UIControl.State.selected)
            extraButtonYes2.addTarget(self, action: #selector(self.extraButton2Tapped), for: .touchUpInside)
            extraButtonNo2.addTarget(self, action: #selector(self.extraButton2Tapped), for: .touchUpInside)
        }
    }
    
    @objc func extraButtonTapped(sender: UIButton) {
        extraButtonNo.backgroundColor = .link
        extraButtonYes.backgroundColor = .link

        sender.backgroundColor = UIColor(named: "customGreen")
        
        let buttonAnswer = sender.currentTitle!
        
        // safe Question Answer
        UserDefaults.standard.set(buttonAnswer, forKey: "extraAnswerOne")
        
        print("extraAnswerOne is \(buttonAnswer)")
    }
    
    @objc func extraButton2Tapped(sender: UIButton) {
        extraButtonNo2.backgroundColor = .link
        extraButtonYes2.backgroundColor = .link

        sender.backgroundColor = UIColor(named: "customGreen")
        
        let buttonAnswer = sender.currentTitle!
        
        // safe Question Answer
        UserDefaults.standard.set(buttonAnswer, forKey: "extraAnswerTwo")
        
        print("extraAnswerTwo is \(buttonAnswer)")
    }
    
    @objc func submitButtonTapped(sender: UIButton) {
        
        let ratingOne = String(format: "%d", self.nmgOne.selectedSegmentIndex)
        UserDefaults.standard.set(ratingOne, forKey: "ratingOne")
        UserDefaults.standard.set(true, forKey: "hasOneRating")
        
        let ratingTwo = String(format: "%d", self.nmgTwo.selectedSegmentIndex)
        UserDefaults.standard.set(ratingTwo, forKey: "ratingTwo")
        UserDefaults.standard.set(true, forKey: "hasTwoRating")
        
        let ratingThree = String(format: "%d", self.nmgThree.selectedSegmentIndex)
        UserDefaults.standard.set(ratingThree, forKey: "ratingThree")
        UserDefaults.standard.set(true, forKey: "hasThreeRating")
        
        // save in CSV
        checkAllData()
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension RatingShort: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
//        liveLabel.text = String(format: "%.0f", self.floatRatingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
//        updatedLabel.text = String(format: "%.0f", self.floatRatingView.rating)
    }
    
}
